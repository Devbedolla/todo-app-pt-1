import React, { Component } from "react";
import todosList from "./todos.json";
//following demo w tj
class App extends Component {
  state = {
    todos: todosList,
    text: "",
  };
  Change=(e)=>{this.setState( {[e.target.name]:e.target.value})}
  addText=(e)=>{
    if(e.keyCode===13){
      const newTodos= this.state.todos
      const newTodo=  {
        "userId": 1,
        "id": Math.random(),
        "title":this.state.text,
        "completed": false
      }
      newTodos.push(newTodo)
      console.log(newTodos)
      console.log(this.state.text)
      this.setState({todos: newTodos, text: ""})
    }
  }
  toggleComplete=(todoid)=>(e)=>{
    const newTodos=this.state.todos
    newTodos.forEach((todo)=>{if(todo.id === todoid){
todo.completed=!todo.completed
    }})
    this.setState({todos:newTodos})
  }
handleDelete=(todoid)=>(e)=>{
  const newTodos=this.state.todos.filter(todo=>todo.id!==todoid)
  this.setState({todos:newTodos})
}
  clearCompleted=()=>{
    const newTodos=this.state.todos.filter(todo=>todo.completed ===false)
    this.setState({todos:newTodos})
  }
  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input className="new-todo" value={this.state.text} name="text" onChange={this.Change} onKeyDown={this.addText} placeholder="What needs to be done?" autoFocus />
        </header>
        <TodoList todos={this.state.todos} 
        toggleComplete={this.toggleComplete}
        handleDelete={this.handleDelete}
        />
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button className="clear-completed" onClick={this.clearCompleted}>Clear completed</button>
        </footer>
      </section>
    );
  }
}

class TodoItem extends Component {
  render() {
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input className="toggle" type="checkbox" checked={this.props.completed}
          onClick={this.props.toggleComplete} />
          <label>{this.props.title}</label>
          <button className="destroy" onClick={this.props.handleDelete}/>
        </div>
      </li>
    );
  }
}

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem title={todo.title} completed={todo.completed}
            toggleComplete={this.props.toggleComplete(todo.id)} 
          handleDelete={this.props.handleDelete(todo.id)}
       />
          ))}
        </ul>
      </section>
    );
  }
}

export default App;
